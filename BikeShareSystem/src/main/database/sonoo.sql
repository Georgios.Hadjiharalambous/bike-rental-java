-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 03, 2019 at 08:16 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.0.33-11+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sonoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `bikes`
--

CREATE TABLE `bikes` (
  `bike_id` int(20) NOT NULL,
  `rented` tinyint(1) NOT NULL DEFAULT '0',
  `damaged` tinyint(1) NOT NULL DEFAULT '0',
  `current_location_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bikes`
--

INSERT INTO `bikes` (`bike_id`, `rented`, `damaged`, `current_location_id`) VALUES
(1, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `location_id` int(20) NOT NULL,
  `address` varchar(20) NOT NULL,
  `postal_code` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`location_id`, `address`, `postal_code`) VALUES
(1, 'glasgow', '456'),
(2, 'glasgow', '456'),
(3, 'Home', 'Home');

-- --------------------------------------------------------

--
-- Table structure for table `rentals`
--

CREATE TABLE `rentals` (
  `rental_id` int(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bike_id` int(11) NOT NULL,
  `location_id_start` int(11) NOT NULL,
  `location_id_end` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT '0000-01-01'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentals`
--

INSERT INTO `rentals` (`rental_id`, `user_id`, `bike_id`, `location_id_start`, `location_id_end`, `date_start`, `date_end`) VALUES
(1, 1, 1, 1, 1, '2019-10-03', '2019-10-03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(40) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `telephone_number` int(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `qualification` varchar(20) NOT NULL DEFAULT 'user',
  `money_credits` int(20) NOT NULL DEFAULT '0',
  `money_due` int(20) NOT NULL DEFAULT '0',
  `age` int(5) NOT NULL DEFAULT '0',
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `full_name`, `telephone_number`, `email`, `qualification`, `money_credits`, `money_due`, `age`, `password`) VALUES
(1, 'Georgios Hadjiharalambous', 99946329, '2486083h@student.gla.ac.uk', 'manager', 0, 0, 0, 'food'),
(2, 'Edo kai ekei', 29, 'gxxshumi', 'user', 0, 162, 25, 'edo'),
(3, 'Sou kiiiii', 121189, 'gxxshumi@nfksgdfgfd', 'user', 0, 0, 25, '44d6feb3466b23a14b1e4f10719365d0e99c'),
(4, 'Sou kiiiii', 354545, 'gxxshumi@nfksgdfgfd', 'user', 0, 0, 25, '44d6feb3466b23a14b1e4f10719365d0e99c'),
(5, 'Sou kiiiii', 354545999, 'gxxshumi@nfksgdfgfd', 'user', 0, 0, 25, '44d6feb3466b23a14b1e4f10719365d0e99c'),
(6, 'Sou kiiiii', 354549, 'gxxshumi@nfksgdfgfd', 'user', 0, 0, 25, 'efb2501bff8f724d3ad9a38de5a7625aea66f48a');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bikes`
--
ALTER TABLE `bikes`
  ADD PRIMARY KEY (`bike_id`),
  ADD KEY `current_location_id` (`current_location_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `rentals`
--
ALTER TABLE `rentals`
  ADD PRIMARY KEY (`rental_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `location_id_start` (`location_id_start`),
  ADD KEY `location_id_end` (`location_id_end`),
  ADD KEY `bike_id` (`bike_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bikes`
--
ALTER TABLE `bikes`
  MODIFY `bike_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `location_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `rentals`
--
ALTER TABLE `rentals`
  MODIFY `rental_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bikes`
--
ALTER TABLE `bikes`
  ADD CONSTRAINT `bikes_ibfk_1` FOREIGN KEY (`current_location_id`) REFERENCES `locations` (`location_id`);

--
-- Constraints for table `rentals`
--
ALTER TABLE `rentals`
  ADD CONSTRAINT `rentals_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `rentals_ibfk_3` FOREIGN KEY (`location_id_start`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `rentals_ibfk_4` FOREIGN KEY (`location_id_end`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `rentals_ibfk_5` FOREIGN KEY (`bike_id`) REFERENCES `bikes` (`bike_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
