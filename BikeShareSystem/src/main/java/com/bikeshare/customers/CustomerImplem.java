package com.bikeshare.customers;

import java.sql.SQLException;
import java.util.HashMap;

import com.bikeshare.database.CustomerDB;

public class CustomerImplem {
	
	public Boolean is_logged_in=false;
	private boolean is_renting = false;

	private int user_id; // we also need user_id for various tasks
	private String email;
	private String full_name;
	private long telephone_number;
	private String qualification;//if its a simple user/manager/operator
	private int money_due=0;
	private int money_credits=0;
	private int age;
	private String password;

	private int rented_bike_id=-1; // -1 if not renting any bike

	private CustomerDB c;


	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getFull_name() {
		return full_name;
	}


	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}


	public long getTelephone_number() {
		return telephone_number;
	}


	public void setTelephone_number(long telephone_number) {
		this.telephone_number = telephone_number;
	}


	public String getQualification() {
		return qualification;
	}


	public void setQualification(String qualification) {
		this.qualification = qualification;
	}


	public int getMoney_due() {
		return money_due;
	}


	public void setMoney_due(int money_due) {
		this.money_due = money_due;
	}


	public int getMoney_credits() {
		return money_credits;
	}


	public void setMoney_credits(int money_credits) {
		this.money_credits = money_credits;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isIs_renting() {
		return is_renting;
	}

	public void setIs_renting(boolean is_renting) {
		this.is_renting = is_renting;
	}

	public int getRented_bike_id() {
		return rented_bike_id;
	}

	public void setRented_bike_id(int rented_bike_id) {
		this.rented_bike_id = rented_bike_id;
	}

	/**
	 * Initialize the data corresponding to the user logged in.
	 */
	public void init(String mail) throws SQLException {
		email=mail;
		HashMap<String, String> rs=c.init(mail);
		user_id=Integer.parseInt(rs.get("user_id"));
		full_name=rs.get("full_name");
		telephone_number=Long.parseLong(rs.get("telephone_number"));
		qualification=rs.get("qualification");
		money_due=Integer.parseInt(rs.get("money_due"));
		money_credits=Integer.parseInt(rs.get("money_credits"));
		age=Integer.parseInt(rs.get("age"));
		rented_bike_id = c.getRentedBike(user_id);
		if(rented_bike_id != -1){
			is_renting = true;
		}


	}
	
	public CustomerDB getCustomerDB() {
		return c;
	}
	/**
	 * Create a new Customer - database connection functionality 
	 */
	public CustomerImplem() {
		 c = new CustomerDB();
	}
	
	

}
