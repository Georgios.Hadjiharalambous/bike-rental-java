package com.bikeshare.customers;

/* Customer details*/
public interface Customer {
	

	/**
	 * Rent a bike at any location in the city, as long as there is a working bike
	 * available at that location.
	 */
	public boolean rentBike(String location, String bikeNumber);

	/**
	 * Return a bike to any location. When a customer returns a bike, their account
	 * is charged an amount depending on how long the bike rental was.
	 */
	public boolean returnBike();

	/**
	 * Report a bike as defective.
	 */
	public void reportBike();

	/**
	 * Pay any charges on their account
	 */
	public void payAmount();

}
