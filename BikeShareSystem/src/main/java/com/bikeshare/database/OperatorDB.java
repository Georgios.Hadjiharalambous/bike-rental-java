package com.bikeshare.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class OperatorDB {
	
	 public static String isbool(String s) {
			if (s.equals("1"))
			return "Yes";
			return "No";		
		}
	 
	 	/*
		 * This method returns from the database all rows containing location, postal_code, address so 
		 * we can report how many locations we have and where exactly.
		 * */
	 
	 	//you have to  check if the data is empty, or i can return also the count number in the hashmap, so you know 
	 	//exactly how many rows there are.
		public static HashMap<String, Object> get_locations_info() {
			String[] columns=null;
		    String[][] data=null;
			Connection con = DBConnection.create_connection_with_db();
			if (con != null) {
				try {
					String query = " Select * from locations ";
					PreparedStatement preparedStmt = con.prepareStatement(query);
					String rowsQuery = "select count(*) from locations";
					PreparedStatement stRows = con.prepareStatement(rowsQuery);
		            ResultSet rsRowsQuery = stRows.executeQuery(rowsQuery);
		            rsRowsQuery.next();
		            int rows = rsRowsQuery.getInt(1);

	                data = new String [rows][4];
	                columns = new String[] {"location id","address","postal_code","station_name"};

					ResultSet rs=preparedStmt.executeQuery();
					int i=0;
					
					while(rs.next()) {
						data[i][0]=rs.getString("location_id");
						data[i][1]=rs.getString("address");
						data[i][2]=rs.getString("postal_code");
						data[i][3]=rs.getString("station_name");

						i++;
					}
					con.close();
				} catch (SQLException e) {
					System.err.println("Got an exception! in get_locations_info");
					e.printStackTrace();
				}
			}
			HashMap<String, Object> datas = new HashMap<String, Object>();
			datas.put("data", data);
			datas.put("columns", columns);
			return datas;
		}
	/*
	 * This method returns from the database all rows containing bike_id, rented, damaged, and current_location_id
	 * rented and damaged a YES/No is return for ease.
	 * */
	 	//you have to  check if the data is empty, or i can return also the count number in the hashmap, so you know 
	 	//exactly how many rows there are.
	public static HashMap<String, Object> get_bike_info() {
		String[] columns=null;
	    String[][] data=null;
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				String query = " Select * from bikes ";
				PreparedStatement preparedStmt = con.prepareStatement(query);
				String rowsQuery = "select count(*) from bikes";
				PreparedStatement stRows = con.prepareStatement(rowsQuery);
	            ResultSet rsRowsQuery = stRows.executeQuery(rowsQuery);
	            rsRowsQuery.next();
	            int rows = rsRowsQuery.getInt(1);

                data = new String [rows][4];
                columns = new String[] {"bike id","damaged","rented","location id"};

				ResultSet rs=preparedStmt.executeQuery();
				int i=0;
				
				while(rs.next()) {
					data[i][0]=rs.getString("bike_id");
					data[i][1]=isbool(rs.getString("damaged"));
					data[i][2]=isbool(rs.getString("rented"));
					data[i][3]=rs.getString("current_location_id");
					i++;
				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in get_bike_info");
				e.printStackTrace();
			}
		}
		HashMap<String, Object> datas = new HashMap<String, Object>();
		datas.put("data", data);
		datas.put("columns", columns);
		return datas;
	}
	/**
	 * The operator changes the bikes location and the database gets updated.
	 */
	public static void change_bike_location(int bike_id, int current_location_id) {
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				String query = " update bikes set current_location_id=? where bike_id = ?";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1, current_location_id);
				preparedStmt.setInt(2, bike_id);
				// execute the preparedstatement
				preparedStmt.executeUpdate();
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in change_bike_location");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * The operator fixes a bike and the database gets updated that the bike is fixed.
	 */	
	public static void fix_damaged_bike(int bike_id) {
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				//1==true ==bike is damaged
				String query = " update bikes set damaged = 0 where bike_id = ?";
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1, bike_id);
				preparedStmt.executeUpdate();
				
				java.sql.Date current_date = new java.sql.Date(new java.util.Date().getTime());

				String queryDamage = " insert into repair (bike_id, repair_date)" + " values (?, ?)";
				PreparedStatement preparedStmtDamage = con.prepareStatement(queryDamage);
				preparedStmtDamage.setInt(1, bike_id);
				preparedStmtDamage.setDate(2,current_date);
				// execute the preparedstatement
				preparedStmtDamage.execute();
				
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in report_damaged_bike");
				e.printStackTrace();
			}
		}
	}

	/**
	 * This method returns from the database all rows containing damage_id, bike_id, damage_date, and damage_detail
	 */
	public static HashMap<String, Object> get_damage_info() {
		String[] columns=null;
		String[][] data=null;
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				String query = " Select * from damage where fixed=0";
				PreparedStatement preparedStmt = con.prepareStatement(query);
				String rowsQuery = "select count(*) from damage where fixed=0";
				PreparedStatement stRows = con.prepareStatement(rowsQuery);
				ResultSet rsRowsQuery = stRows.executeQuery(rowsQuery);
				rsRowsQuery.next();
				int rows = rsRowsQuery.getInt(1);

				data = new String [rows][4];
				columns = new String[] {"damage_id","bike_id","damage_date","damage_details"};

				ResultSet rs=preparedStmt.executeQuery();
				int i=0;

				while(rs.next()) {
					data[i][0]=rs.getString("damage_id");
					data[i][1]=rs.getString("bike_id");
					data[i][2]=rs.getString("damage_date");
					data[i][3]=rs.getString("damage_details");
					i++;
				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in get_damage_info");
				e.printStackTrace();
			}
		}
		HashMap<String, Object> datas = new HashMap<String, Object>();
		datas.put("data", data);
		datas.put("columns", columns);
		return datas;
	}
	/**
	 * The operator solve a report and update the database so the solved report won't be shown again
	 */
	public static void solve_report(int damage_id) {
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				//1==true == damage has been repaired
				String query = " update damage set fixed = 1 where damage_id = ?";
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1, damage_id);
				preparedStmt.executeUpdate();

				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in report_damaged_bike");
				e.printStackTrace();
			}
		}
	}
	//just toy example to see if graphs are working
	//you can check it out by calling the function 
	//useless in general
	/*
	public static void  graph() {
		DefaultCategoryDataset dataset=new DefaultCategoryDataset();
		dataset.setValue(1,"g","asbes"); 
		dataset.setValue(34, "g", "girogos");
		dataset.setValue(46, "g", "xriso");

		JFreeChart chart=ChartFactory.createLineChart("title test","x axis title","y axis title",dataset,PlotOrientation.HORIZONTAL, false, false, false);
		CategoryPlot p=chart.getCategoryPlot();
		p.setRangeGridlinePaint(Color.black);
		ChartFrame frame = new ChartFrame("Bar chart",chart);
		frame.setVisible(true);
		frame.setSize(500, 500);

	}
	
		*/

	
		
		
		
		
		
	
	
	
	/**
	 * If we would like to implement addition of new bikes in the database
	 * by the Operator -> EXTRA FUNCTIONALITY.
	 * The operator adds a new bikes to the database,
	 */	
	public static void add_bike(int current_location_id) {// maybe we can check if the postal code is unique
		// in the world,and use that as primary key
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				String query = " insert into bikes (current_location_id)" + " values (?)";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1, current_location_id);

				// execute the preparedstatement
				preparedStmt.execute();
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in add_bike");
				e.printStackTrace();
			}
		}
	}

}

