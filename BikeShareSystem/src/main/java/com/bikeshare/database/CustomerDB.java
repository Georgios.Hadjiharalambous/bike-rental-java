package com.bikeshare.database;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomerDB {
	
	
	/*
	 * Checks if a given email is already in the database, so we do not have duplicate . 
	 * */
	public static Boolean check_email_exist(String email) {
		
		Connection con = DBConnection.create_connection_with_db();
		int count=0;

		if (con != null) {
			try {
				String query = " Select count(*) from users where email=?";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setString(1, email);
				// execute the preparedstatement
				ResultSet rs=preparedStmt.executeQuery();
				while(rs.next()) {
					count=rs.getInt(1);
					break;
				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in check_login");
				e.printStackTrace();
			}
		}
		return count>0;		
	}
	/**
	 * Returns a HashMap with the data of the user with this specific email.
	 */
	public static HashMap<String, String> init(String email) {
		Connection con = DBConnection.create_connection_with_db();
		HashMap<String, String> capitalCities = new HashMap<String, String>();

		ResultSet rs=null;
		if (con != null) {
			try {
				String query = " Select user_id,full_name,telephone_number,qualification,money_due,money_credits,age from users where email=?";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setString(1, email);
				// execute the preparedstatement
				rs=preparedStmt.executeQuery();
				while(rs.next()) {
					capitalCities.put("user_id",rs.getString("user_id"));
					capitalCities.put("full_name",rs.getString("full_name"));
					capitalCities.put("telephone_number",rs.getString("telephone_number"));
					capitalCities.put("qualification",rs.getString("qualification"));
					capitalCities.put("money_due",rs.getString("money_due"));
					capitalCities.put("money_credits",rs.getString("money_credits"));
					capitalCities.put("age",rs.getString("age"));
					break;
				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in init customerDB");
				e.printStackTrace();
			}
		}
		return capitalCities;
	}
	/**
	 *  Get bike_id that user is currently renting, -1 if not renting any
	 */
	public static int getRentedBike(int userID) {
		Connection con = DBConnection.create_connection_with_db();
		int rentedBikeId =-1;
		ResultSet rs=null;
		if (con != null) {
			try {
				String query = " select bike_id FROM rentals WHERE location_id_end is NULL AND user_id=?";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setString(1, String.valueOf(userID));
				// execute the preparedstatement
				rs=preparedStmt.executeQuery();
				while(rs.next()) {
					rentedBikeId = rs.getInt("bike_id");
					break;
				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in getRentedBike customerDB");
				e.printStackTrace();
			}
		}
		return rentedBikeId;
	}

	/**
	 *Given an email and a password, checks if the combination corresponds to a user
	 *and returns a true/false. 
	 */
	
	public static Boolean check_login(String email,String pass) throws NoSuchAlgorithmException {
		//pass=Hashpassword.hashPassword(pass);
		Connection con = DBConnection.create_connection_with_db();
		String dbPass=null;

		if (con != null) {
			try {
				String query = " Select password from users where email=?";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setString(1, email);
				// execute the preparedstatement
				ResultSet rs=preparedStmt.executeQuery();
				while(rs.next()) {
					dbPass=rs.getString("password");
					break;
				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in check_login");
				e.printStackTrace();
			}
		}

		return Hashpassword.confirmPassword(pass, dbPass);
	}
	
	/**
	 * Takes as inputs the user's data and puts it in the database
	 */
	public static void add_user(String full_name, long telephone_number, String email, int age, String password) {
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				password=Hashpassword.hashPassword(password);

				String query = " insert into users (full_name, telephone_number, email, age,password)"
						+ " values (?, ?, ?, ?, ?)";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setString(1, full_name);
				preparedStmt.setLong(2, telephone_number);
				preparedStmt.setString(3, email);
				preparedStmt.setInt(4, age);
				preparedStmt.setString(5, password);
				// execute the preparedstatement
				preparedStmt.execute();
				con.close();
			} catch (SQLException | NoSuchAlgorithmException e) {
				System.err.println("Got an exception! in add_user");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * If we implement the manager/operator to add a new location to  the database
	 */
	public static void add_location(String address, String postal_code,String station_name) {
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				String query = " insert into locations (address, postal_code,station_name)" + " values (?, ?,?)";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setString(1, address);
				preparedStmt.setString(2, postal_code);
				preparedStmt.setString(3, station_name);			
				
				// execute the preparedstatement
				preparedStmt.execute();
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in add_location");
				e.printStackTrace();
			}
		}
	}
	/**
	 * Return the map of location_ids and lists of available bike_ids
	 */
	public static HashMap<Integer, ArrayList<Integer>> get_available_bikes() {
		Connection con = DBConnection.create_connection_with_db();
		HashMap<Integer,ArrayList<Integer>> bikes = new HashMap<Integer, ArrayList<Integer>>();
		ResultSet rs=null;
		if (con != null) {
			try {
				//1==true ==bike is rented
				String query = "select bike_id, current_location_id from bikes where rented=0 and damaged=0";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				// execute the preparedstatement
				rs = preparedStmt.executeQuery();
				while(rs.next()) {
					int location_id = rs.getInt("current_location_id");
					int bike_id = rs.getInt("bike_id");
					if(bikes.containsKey(location_id)){
						bikes.get(location_id).add(bike_id);
						continue;
					}
					bikes.put(location_id, new ArrayList<Integer>());
					bikes.get(location_id).add(bike_id);
					//break;i think this is not needed.
				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in rent_a_bike");
				e.printStackTrace();
			}
		}
		return bikes;
	}
	/**
	 * Return all location_ids for displaying locations in the combo-box
	 */
	public static HashMap<Integer, String> get_location_ids_name() {
		Connection con = DBConnection.create_connection_with_db();
		HashMap<Integer, String> locations = new HashMap<Integer, String>();
		ResultSet rs=null;
		if (con != null) {
			try {
				//1==true ==bike is rented
				String query = " select location_id, station_name from locations";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				// execute the preparedstatement
				rs = preparedStmt.executeQuery();
				while(rs.next()) {
					int loc_id = rs.getInt("location_id");
					String loc_name = rs.getString("station_name");
					locations.put(loc_id,loc_name);
				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in get_location_ids");
				e.printStackTrace();
			}
		}
		return locations;
	}
	/**
	 * We set that a specific bike is rented in the database
	 */
	public static void rent_a_bike(int bike_id) {
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				//1==true ==bike is rented
				String query = " update bikes set rented = 1 where bike_id = ?";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1, bike_id);

				// execute the preparedstatement
				preparedStmt.executeUpdate();
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in rent_a_bike");
				e.printStackTrace();
			}
		}

	}
	/**
	 * We set that a specific bike is damaged in the database
	 */
	public static void report_damaged_bike(int bike_id,String details) {
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				//1==true ==bike is damaged
				String query = " update bikes set damaged = 1 where bike_id = ?";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1, bike_id);

				// execute the preparedstatement
				preparedStmt.executeUpdate();
				java.sql.Date current_date = new java.sql.Date(new java.util.Date().getTime());

				String queryDamage = " insert into damage (bike_id, damage_date,damage_details)" + " values (?, ?,?)";
				PreparedStatement preparedStmtDamage = con.prepareStatement(queryDamage);
				preparedStmtDamage.setInt(1, bike_id);
				preparedStmtDamage.setDate(2,current_date);
				preparedStmtDamage.setString(3, details);
				// execute the preparedstatement
				preparedStmtDamage.execute();
				
				
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in report_damaged_bike");
				e.printStackTrace();
			}
		}

	}
	/**
	 * We create a new rental in the database
	 */
	public static void add_rental(int user_id,int bike_id, int location_id_start) {
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				String query = " insert into rentals (user_id, bike_id, location_id_start, date_start)"
						+ " values (?, ?, ?, ?)";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1, user_id);
				preparedStmt.setInt(2, bike_id);
				preparedStmt.setInt(3, location_id_start);
				java.sql.Date date_start = new java.sql.Date(new java.util.Date().getTime());
				preparedStmt.setDate(4, date_start);
				//preparedStmt.setInt(4, date_start);
				// execute the preparedstatement
				preparedStmt.execute();
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in add_rental");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * The users delivers the bike to a  location and the database gets updated 
	 * with the new location of the bike, and that the bike is now not rented.
	 */
	public static void deliver_a_bike(int bike_id, int current_location_id) {
		Connection con = DBConnection.create_connection_with_db();
		if (con != null) {
			try {
				String query = " update bikes set rented = 0,current_location_id=? where bike_id = ?";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1, current_location_id);
				preparedStmt.setInt(2, bike_id);
				// execute the preparedstatement
				preparedStmt.executeUpdate();
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in deliver_a_bike");
				e.printStackTrace();
			}
		}
	}
	/**
	 * Given the days the user rented the bike, we calculate the amount
	 * he needs to pay.
	 */
	public static int find_charge_for_days(Long days) {
		if(days<=0)
			days=new Long(1);
		int money =0;
		if (days<30)
			money=(int) (days*6);
		else money=(int)(days-30)*4+140;//policy-> if more than one month, firsth month 50 pounds and then 3 p per day
		if (days>365/2)
			money=(int) (days-365/2)*3+600;
		
		return money;
	}
	
	/**
	 * We update the rental that just finished with the location that ended the trip.
	 * Also we update the money that the user needs to pay.
	 * + Also return money so we can display the charge to the customer
	 */
	public static int finished_rental(int user_id, int location_id_end) throws ParseException {
		Connection con = DBConnection.create_connection_with_db();
		int money =0;
		if (con != null) {
			try {
				java.sql.Date date_end=null;
				Long range =0l;
				java.sql.Date date_start=null;
				String date_start_String=null;
				String qDate=" select date_start from rentals where user_id=? and date_end='0000-01-01'";
				PreparedStatement preparedStmt0 = con.prepareStatement(qDate);
				preparedStmt0.setInt(1,user_id);
				ResultSet rs=preparedStmt0.executeQuery();
				
				while (rs.next()) {
					date_start_String = rs.getString("date_start");
					break;
				}
				if(date_start_String!=null) {					
					date_end = new java.sql.Date(new java.util.Date().getTime());		
					LocalDate startDate = LocalDate.parse(date_start_String);
			        LocalDate endtDate = LocalDate.now();
			        range = ChronoUnit.DAYS.between(startDate, endtDate);
				}				

				money=find_charge_for_days(range);
				
				
				//here the email could be given as an input instead of this
				String email=null;
				String emailQ="Select email from users where user_id=?";
				PreparedStatement preparedStmt1 = con.prepareStatement(emailQ);
				preparedStmt1.setInt(1,user_id);
				ResultSet rsEmail=preparedStmt1.executeQuery();
				while(rsEmail.next()) {
					email=rsEmail.getString("email");
					break;
				}
				
				add_money_due(money,email);//updates the money to pay
				//code needs fixing, must be checked if failed, maybe insight if clause

				String query = " update rentals set location_id_end=?, date_end=?, money_cost=? where user_id=? and date_end='0000-01-01'";
				// create the mysql insert preparedstatement
				PreparedStatement preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1, location_id_end);
				preparedStmt.setDate(2, date_end);
				preparedStmt.setInt(3, money);
				preparedStmt.setInt(4, user_id);
				// execute the preparedstatement
				preparedStmt.execute();
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in finished_rental");
				e.printStackTrace();
			}
		}
		return money;
	}
	/**
	 * We update the money the user has in its wallet in the database.(add money to wallet if he 
	 * has no money due, or just deduct from the money he owns)
	 */
	public static void add_money_credits(int number, String email) {
		Connection con = DBConnection.create_connection_with_db();
		int money_due = 0;
		int money_credits = 0;

		if (con != null) {
			try {
				String Qmoney_due = "Select money_due,money_credits from users where email=?";
				PreparedStatement preparedStmt = con.prepareStatement(Qmoney_due);
				preparedStmt.setString(1, email);
				ResultSet rs = preparedStmt.executeQuery();
				while (rs.next()) {
					money_due = Integer.parseInt(rs.getString("money_due"));
					money_credits = Integer.parseInt(rs.getString("money_credits"));
				}
				if (money_due == 0) {
					number = number + money_credits;
					String query = "update users set money_credits = ? where email = ?";
					PreparedStatement preparedStmt1 = con.prepareStatement(query);
					preparedStmt1.setInt(1, number);
					preparedStmt1.setString(2, email);
					// execute the java preparedstatement
					preparedStmt1.executeUpdate();
				} else {
					if (money_due >= number) {
						money_due = money_due - number;
						number = 0;
					} else {
						number = number - money_due;
						money_due = 0;
					}
					String query = "update users set money_credits = ?,money_due = ? where email = ?";
					PreparedStatement preparedStmt1 = con.prepareStatement(query);
					preparedStmt1.setInt(1, number);
					preparedStmt1.setInt(2, money_due);
					preparedStmt1.setString(3, email);
					// execute the java preparedstatement
					preparedStmt1.executeUpdate();

				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in add_money");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * We update the money the user has in its wallet in the database.(remove money 
	 * if they exist or just add to the money due)
	 */
	public static void add_money_due(int number, String email) {
		Connection con = DBConnection.create_connection_with_db();
		int money_due = 0;
		int money_credits = 0;

		if (con != null) {
			try {
				String Qmoney_due = "Select money_due,money_credits from users where email=?";
				PreparedStatement preparedStmt = con.prepareStatement(Qmoney_due);
				preparedStmt.setString(1, email);
				ResultSet rs = preparedStmt.executeQuery();
				while (rs.next()) {
					money_due = Integer.parseInt(rs.getString("money_due"));
					money_credits = Integer.parseInt(rs.getString("money_credits"));
				}
				if (money_credits == 0) {// if he doesnt have any money in the wallet
					number = number + money_due;
					String query = "update users set money_due = ? where email = ?";
					PreparedStatement preparedStmt1 = con.prepareStatement(query);
					preparedStmt1.setInt(1, number);
					preparedStmt1.setString(2, email);
					// execute the java preparedstatement
					preparedStmt1.executeUpdate();
				} else {
					if (money_credits >= number) {
						money_credits = money_credits - number;
						number = 0;
					} else {
						number = number - money_credits;
						money_credits = 0;
					}
					String query = "update users set money_credits = ?,money_due = ? where email = ?";
					PreparedStatement preparedStmt1 = con.prepareStatement(query);
					preparedStmt1.setInt(1, money_credits);
					preparedStmt1.setInt(2, number);
					preparedStmt1.setString(3, email);
					// execute the java preparedstatement
					preparedStmt1.executeUpdate();

				}
				con.close();
			} catch (SQLException e) {
				System.err.println("Got an exception! in add_money");
				e.printStackTrace();
			}
		}
	}
}
