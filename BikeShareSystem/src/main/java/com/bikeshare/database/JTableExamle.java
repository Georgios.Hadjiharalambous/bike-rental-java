package com.bikeshare.database;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
//Packages to import 
class JTableExamples { 
	// frame 
	JFrame f; 
	// Table 
	JTable j; 	
	 String[] columns=null;
	    String[][] data=null;
	    public String isbool(String s) {
			if (s.equals("1"))
			return "Yes";
			return "No";		
		}
	
//Constructor 
		JTableExamples() 
		{ 
			Connection con = DBConnection.create_connection_with_db();
			if (con != null) {
				try {
					String query = " Select * from bikes ";
					// create the mysql insert preparedstatement
					PreparedStatement preparedStmt = con.prepareStatement(query);
					// execute the preparedstatement
					String rowsQuery = "select count(*) from bikes";
					PreparedStatement stRows = con.prepareStatement(rowsQuery);
		            ResultSet rsRowsQuery = stRows.executeQuery(rowsQuery);
		            rsRowsQuery.next();
		            int rows = rsRowsQuery.getInt(1);

	                data = new String [rows][4];
	                columns = new String[] {"bike id","damaged","rented","location id"};

					ResultSet rs=preparedStmt.executeQuery();
					int i=0;
					
					while(rs.next()) {
						data[i][0]=rs.getString("bike_id");
						data[i][1]=isbool(rs.getString("damaged"));
						data[i][2]=isbool(rs.getString("rented"));
						data[i][3]=rs.getString("current_location_id");
						i++;
					}
					con.close();
				} catch (SQLException e) {
					System.err.println("Got an exception! in add_bike");
					e.printStackTrace();
				}
			}
			// Frame initiallization 
			f = new JFrame(); 

			// Frame Title 
			f.setTitle("JTable Example"); 

			
			// Initializing the JTable 
			j = new JTable(data, columns); 
			j.setBounds(30, 40, 200, 300); 

			// adding it to JScrollPane 
			JScrollPane sp = new JScrollPane(j); 
			f.add(sp); 
			// Frame Size 
			f.setSize(500, 200); 
			// Frame Visible = true 
			f.setVisible(true); 
		}
		
		
		

		// Driver method 
		public static void main(String[] args) 
		{ 
			new JTableExamples(); 
		} 
	} 