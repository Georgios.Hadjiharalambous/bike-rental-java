package com.bikeshare.database;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	/**
	 * Creates and returns a connection with the database
	 */
	public static Connection create_connection_with_db() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/BikeRental", "root", "root");
			//con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sonoo", "root", "skupidi"); // here sonoo is
																										// database
		} catch (Exception e) {
			System.out.println(e);

		}
		return con;
	}
}
