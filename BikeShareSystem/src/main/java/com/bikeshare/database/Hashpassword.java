package com.bikeshare.database;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hashpassword {

	/**
	 * This method implements password hashing. The algorithm used is SHA.
	 * Passwords, are wrong to be saved without being hashed first, as in chance of
	 * hacking the database, all of the users passwords, will be known. Instead,
	 * with hashing the authentication is done and the passwords are safe at the
	 * same time.
	 */

	public static String hashPassword(String password) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA");
		md.update(password.getBytes());
		byte[] b = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b1 : b) {
			sb.append(Integer.toHexString(b1 & 0xff).toString());
		}
		return sb.toString();
	}

	/**
	 * This method, is used to confirm whether, the password entered from the
	 * user,is the same as the one stored in the database for this particular user.
	 * Returns true/false.
	 */
	public static boolean confirmPassword(String pass, String dbPass) {
		String hashedPassword = null;
		try {
			hashedPassword = hashPassword(pass);
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e);
		}

		if (hashedPassword.equals(dbPass))
			return true;
		else
			return false;
	}

}
