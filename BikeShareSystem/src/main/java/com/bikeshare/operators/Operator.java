package com.bikeshare.operators;

public interface Operator {

	/**
	 * Track the location of all bikes in the city.
	 */
	public void trackLocation();

	/**
	 * Repair a defective bike.
	 */
	public void repairBike();

	/**
	 * Move bikes to different locations around the city as needed
	 */
	public void moveBike();
}
