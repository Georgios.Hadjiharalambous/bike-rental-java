package com.bikeshare.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Wallet extends JFrame implements ActionListener {

    //GUI Components
    JLabel currentBalance = new JLabel("Your current Balance is");
    JLabel balance = new JLabel("£: ");

    JLabel topupLabel = new JLabel("Top up amount : £");
    JTextField topupText = new JTextField(40);

    JButton topupButton =new JButton("Top Up!");
    JButton payButton =new JButton("Pay Due");


    JButton backButton = new JButton("Back");
    JPanel balancePanel = new JPanel();
    JLabel menuLabel = new JLabel("My Wallet");
    JPanel topPanel = new JPanel();
    // Customer instance
    com.bikeshare.customers.CustomerImplem customer = init_app.menu.customer;

    public Wallet(){

        setLayout(null);
        balancePanel.setBounds(0,50,800,150);
        balancePanel.setBackground(Color.decode("#f1f0ee"));
        balancePanel.setLayout(null);
        currentBalance.setBounds(320,0,230,30);
        currentBalance.setFont(new Font(Font.SANS_SERIF, Font.ITALIC,16));
        balance.setBounds(320,30,400,100);
        balance.setFont(new Font(Font.SANS_SERIF, Font.BOLD,42));
        balancePanel.add(currentBalance);
        balancePanel.add(balance);

        topupLabel.setBounds(250,250,150,30);
        topupLabel.setFont(new Font(Font.SERIF, Font.PLAIN,18));
        topupText.setBounds(400,250,100,30);
        topupButton.setBounds(300,300,200,60);
        payButton.setBounds(300,380,200,60);

        menuLabel.setForeground(Color.decode("#f1f0ee"));
        menuLabel.setFont(new Font(Font.SERIF, Font.BOLD,28));
        menuLabel.setBounds(10,10,200,30);
        menuLabel.setHorizontalAlignment(JLabel.CENTER);
        topPanel.setBounds(0,0,800,50);
        topPanel.setBackground(Color.decode("#269ccc"));
        topPanel.setLayout(null);
        backButton.setBounds(700,10,80,30);

        topPanel.add(backButton);
        topPanel.add(menuLabel);
        add(topPanel);
        add(balancePanel);

        add(topupLabel);
        add(topupText);
        add(topupButton);
        add(payButton);

        getContentPane().setBackground(Color.WHITE);
        setLocationByPlatform(true);
        setVisible(true);
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        if(customer.getMoney_due()>0){
            currentBalance.setText("Your current due is ");
            balance.setText(String.format("£: %d.00 ", customer.getMoney_due()));
            balance.setForeground(Color.decode("#c53211"));
        }else{
            payButton.setEnabled(false);
            balance.setText(String.format("£: %d.00 ", customer.getMoney_credits()));
        }
        // add money to customer credits then update balance
        topupButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(!confirmPayment()){
                    topupText.setText("");
                    return;
                }
                int topupAmount = Integer.parseInt(topupText.getText());
                customer.getCustomerDB().add_money_credits(topupAmount, customer.getEmail());

                if(topupAmount - customer.getMoney_due() >=0){
                    customer.setMoney_credits(customer.getMoney_credits()+topupAmount-customer.getMoney_due());
                    customer.setMoney_due(0);
                    balance.setText(String.format("£: %d.00 ", customer.getMoney_credits()));
                    currentBalance.setText("Your current Balance is ");
                    balance.setForeground(Color.BLACK);

                }else{
                    customer.setMoney_due(customer.getMoney_due()-topupAmount);
                    balance.setText(String.format("£: %d.00 ", customer.getMoney_due()));
                }
                topupText.setText("");
            }
        });
        // pay the amount due
        payButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(!confirmPayment()) return;
                int topupAmount = customer.getMoney_due();
                customer.getCustomerDB().add_money_credits(topupAmount, customer.getEmail());

                customer.setMoney_due(0);
                balance.setText(String.format("£: %d.00 ", customer.getMoney_credits()));
                currentBalance.setText("Your current Balance is ");
                balance.setForeground(Color.BLACK);

                topupText.setText("");
            }
        });
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new CustomerMenu().setVisible(true);
                dispose();
            }
        });

    }
    private  boolean confirmPayment(){

        int payment_result = JOptionPane.showConfirmDialog (null, "Confirm your payment?","Confirmation",JOptionPane.YES_NO_OPTION);

        if(payment_result == JOptionPane.YES_OPTION){
            JOptionPane.showMessageDialog(null, "Insert your credit card, input your PIN number then press OK");
            JOptionPane.showMessageDialog(null, "Thank you, your payment was successful");
            return true;
        }
        if(payment_result == JOptionPane.NO_OPTION){
            JOptionPane.showMessageDialog(null, "Your transaction has been cancelled");
        }
        return false;
    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }
}
