package com.bikeshare.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class CustomerMenu extends JFrame implements ActionListener {

    JButton rentButton =new JButton("Rent Bike");
    JButton returnButton =new JButton("Return Bike");
    JButton walletButton =new JButton("My Wallet");
    JButton reportButton =new JButton("Report Damage");
    JButton logoutButton =new JButton("Logout");

    // Customer instance
    com.bikeshare.customers.CustomerImplem customer = init_app.menu.customer;

    public CustomerMenu() {


        setLayout(null);
        logoutButton.setBounds(650,30,80,30);
        rentButton.setBounds(250,100,300,60);
        returnButton.setBounds(250,200,300,60);
        walletButton.setBounds(250,300,300,60);
        reportButton.setBounds(250,400,300,60);

        add(rentButton);
        add(returnButton);
        add(walletButton);
        add(reportButton);
        add(logoutButton);

        getContentPane().setBackground(Color.WHITE);
        setLocationByPlatform(true);
        setVisible(true);
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);//close on exit

        rentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(customer.getRented_bike_id() != -1){
                    JOptionPane.showMessageDialog(null,"Please return your bike before renting another bike");
                    return;
                }
                dispose();
                new RentBike().setVisible(true);
                // update bikes list every time the card is shown
            }
        });
        returnButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(customer.getRented_bike_id() == -1){
                    JOptionPane.showMessageDialog(null,"You have no bike to return");
                    return;
                }
                dispose();
                new ReturnBike().setVisible(true);
                // update bikes list every time the card is shown
            }
        });
        walletButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
                new Wallet().setVisible(true);
                // update bikes list every time the card is shown
            }
        });
        reportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
                new ReportBrokenBike().setVisible(true);
                // update bikes list every time the card is shown
            }
        });
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
                new Menu().setVisible(true);
                // update bikes list every time the card is shown
            }
        });

    }


    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Register().setVisible(true);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }
}
