package com.bikeshare.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WelcomeCustomer extends JPanel implements ActionListener {

    public WelcomeCustomer(){

        JLabel welcomeLabel = new JLabel("WELCOME");
        welcomeLabel.setBounds(200,300,200,100);
        add(welcomeLabel);
        setBackground(Color.WHITE);
    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }
}
