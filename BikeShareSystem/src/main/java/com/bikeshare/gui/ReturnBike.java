package com.bikeshare.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

public class ReturnBike extends JFrame implements ActionListener {
    // GUI components
    JButton returnButton =new JButton("Return Bike");
    JLabel locationLabel = new JLabel("Return to");
    JComboBox locationCombo = new JComboBox();

    JLabel menuLabel = new JLabel("Return Bike");
    JButton backButton = new JButton("Back");
    JPanel topPanel = new JPanel();
    // Customer instance
    com.bikeshare.customers.CustomerImplem customer = init_app.menu.customer;

    public ReturnBike(){
        setLayout(null);

        locationLabel.setBounds(200,200,200,30);
        locationCombo.setBounds(200,230,400,30);
        returnButton.setBounds(300,400,200,30);
        backButton.setBounds(600,30,100,30);

        menuLabel.setForeground(Color.decode("#f1f0ee"));
        menuLabel.setFont(new Font("Sans Serif", Font.BOLD,28));
        menuLabel.setBounds(10,10,200,30);
        menuLabel.setHorizontalAlignment(JLabel.CENTER);
        topPanel.setBounds(0,0,800,50);
        topPanel.setBackground(Color.decode("#269ccc"));
        topPanel.setLayout(null);
        backButton.setBounds(700,10,80,30);
        topPanel.add(backButton);
        topPanel.add(menuLabel);

        add(topPanel);
        add(locationLabel);
        add(locationCombo);
        add(returnButton);

        getContentPane().setBackground(Color.WHITE);
        setLocationByPlatform(true);
        setVisible(true);
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        Map<Integer,String> locations  = customer.getCustomerDB().get_location_ids_name();
        // Add location options to the combo box
        String instructionMessage = " - Please select a location - ";
        locationCombo.addItem(instructionMessage);
        for(int location_id : locations.keySet()){
            locationCombo.addItem(location_id +": "+locations.get(location_id));
        }

        // enable return button when a location is selected
        locationCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if(locationCombo.getSelectedIndex() != -1){
                    locationCombo.setEnabled(true);
                }
            }
        });

        //Finish rental then update bike status.
        returnButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    int bike_id = customer.getRented_bike_id();
                    int user_id = customer.getUser_id();
                    String location_name = (String)locationCombo.getSelectedItem();
                    int location_id = Integer.parseInt(location_name.substring(0,location_name.indexOf(":")));
                    int money = customer.getCustomerDB().finished_rental(user_id, location_id);
                    customer.getCustomerDB().deliver_a_bike(bike_id, location_id);
                    customer.setIs_renting(false);
                    customer.setRented_bike_id(-1);
                    if(customer.getMoney_credits() >= money){
                        JOptionPane.showMessageDialog(null, "Total charge : £" + money +
                                "\nAutomatically deducted from your wallet.");
                        customer.setMoney_credits(customer.getMoney_credits()-money);
                    }else{
                        JOptionPane.showMessageDialog(null, "Total charge : £" + money +
                                "\nPlease go to Wallet to pay the outstanding charges.");
                        customer.setMoney_due(money-customer.getMoney_credits());
                        customer.setMoney_credits(0);
                    }
                    new CustomerMenu().setVisible(true);
                    dispose();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });

        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new CustomerMenu().setVisible(true);
                dispose();
            }
        });

    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }

}
