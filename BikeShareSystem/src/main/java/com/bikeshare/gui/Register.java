package com.bikeshare.gui;


import com.bikeshare.customers.CustomerImplem;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.swing.JLabel;
import javax.swing.JFrame;

public class Register extends JFrame implements ActionListener {

    JLabel firstNameLabel = new JLabel("First Name");
    JTextField firstNameText =new JTextField(40);

    JLabel lastNameLabel = new JLabel("Last Name");
    JTextField lastNameText =new JTextField(40);

    JLabel emailLabel = new JLabel("Email Address *");
    JTextField emailText =new JTextField(40);

    JLabel passwordLabel = new JLabel("Password *");
    JTextField passwordText =new JPasswordField(40);

    JLabel confirmLabel = new JLabel("Confirm password *");
    JTextField confirmText =new JPasswordField(40);

    JLabel phoneLabel = new JLabel("Phone number");
    JTextField phoneText =new JTextField(40);

    JLabel dobLabel = new JLabel("Age");
    JTextField dobText =new JTextField(40);

    JButton registerButton =new JButton("Register");
    JButton backButton =new JButton("Back");

    JPanel contentPane = new JPanel();

    public Register(){
        CustomerImplem customer = init_app.menu.customer;

        JFrame frame = new JFrame();
        setTitle("Register");
        setSize(600,600);
        setLocation(400,100);

        JPanel panel = new JPanel();
        try {
            BufferedImage logoImage = ImageIO.read(new File("src/main/java/com/bikeshare/gui/pic/logo.jpg"));
            JLabel logo = new JLabel(new ImageIcon(logoImage.getScaledInstance(170,170, Image.SCALE_SMOOTH)));
            add(logo);
            logo.setBounds(220,40,170,170);
        }catch(Exception e){
            e.printStackTrace();
        }

        Font font = new Font(Font.MONOSPACED, Font.PLAIN, 14);

        JLabel firstNameLabel = new JLabel("First Name",JLabel.RIGHT);
        JTextField firstName = new JTextField(40);
        JLabel lastNameLabel = new JLabel("Last Name",JLabel.RIGHT);
        JTextField lastName =new JTextField(40);
        JLabel emailLabel = new JLabel("Email Address *",JLabel.RIGHT);
        JTextField email =new JTextField(40);
        JLabel passwordLabel = new JLabel("Password *",JLabel.RIGHT);
        JPasswordField password =new JPasswordField(40);
        password.setEchoChar('*');
        JLabel confirmLabel = new JLabel("Confirm Password *",JLabel.RIGHT);
        JPasswordField confirm =new JPasswordField(40);
        confirm.setEchoChar('*');
        JLabel phoneLabel = new JLabel("Phone Number(+44)",JLabel.RIGHT);
        JTextField phone =new JTextField(40);
        JLabel doBLabel = new JLabel("Age",JLabel.RIGHT);
        JTextField doB =new JTextField(40);

        firstNameLabel.setFont(font);
        lastNameLabel.setFont(font);
        emailLabel.setFont(font);
        passwordLabel.setFont(font);
        confirmLabel.setFont(font);
        phoneLabel.setFont(font);
        doBLabel.setFont(font);

        firstNameLabel.setBounds(30, 240, 180, 30);
        lastNameLabel.setBounds(30, 278, 180, 30);
        emailLabel.setBounds(30, 316, 180, 30);
        passwordLabel.setBounds(30, 354, 180, 30);
        confirmLabel.setBounds(30, 392, 180, 30);
        phoneLabel.setBounds(30, 430, 180, 30);
        doBLabel.setBounds(30, 468, 180, 30);

        firstName.setBounds(220, 240, 280, 30);
        lastName.setBounds(220, 278, 280, 30);
        email.setBounds(220, 316, 280, 30);
        password.setBounds(220, 354, 280, 30);
        confirm.setBounds(220, 392, 280, 30);
        phone.setBounds(220, 430, 280, 30);
        doB.setBounds(220, 468, 280, 30);

        JButton registerButton =new JButton("Register");
        registerButton.setFont(font);
        registerButton.setBounds(135, 520, 155, 33);

        JButton backButton =new JButton("Back");
        backButton.setFont(font);
        backButton.setBounds(350, 520, 150, 33);

        add(firstNameLabel);
        add(lastNameLabel);
        add(emailLabel);
        add(passwordLabel);
        add(confirmLabel);
        add(phoneLabel);
        add(doBLabel);

        add(firstName);
        add(lastName);
        add(email);
        add(password);
        add(confirm);
        add(phone);
        add(doB);

        add(registerButton);
        add(backButton);

        getContentPane().setBackground(Color.WHITE);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // check if passwords match then add a new user
        registerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(customer.getCustomerDB().check_email_exist(email.getText())){
                    JOptionPane.showMessageDialog(null,"An account with this email already exists");
                    return;
                }
                if(!password.getText().equals(confirm.getText())){
                    JOptionPane.showMessageDialog(null,"Password does not match confirm password.");
                    return;
                }
                String fullName = firstName.getText() + lastName.getText();
                customer.getCustomerDB().add_user(fullName, Long.parseLong(phone.getText()), email.getText(), Integer.parseInt(doB.getText()), password.getText());
                dispose();

                init_app.menu.setVisible(true);
            }
        });
        
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
                init_app.menu.setVisible(true);
            }
        });
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Register().setVisible(true);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }

}



