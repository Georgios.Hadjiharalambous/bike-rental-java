package com.bikeshare.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.*;

import com.bikeshare.customers.CustomerImplem;
import com.bikeshare.database.OperatorDB;

public class Menu extends JFrame implements ActionListener {
	JLabel emailLabel = new JLabel("Email Adress");
	JLabel passwordLabel = new JLabel("Password");
	JTextField email = new JTextField(40);
	JPasswordField password = new JPasswordField(40);
	JButton loginButton = new JButton("Log in");
	JButton registerButton = new JButton("Register");
	JButton infoButton = new JButton("Information");

	public CustomerImplem customer;
	public OperatorDB operator;
	public Menu() {

		customer= new CustomerImplem();//initialization
		operator = new OperatorDB();


		try {
			BufferedImage logoImage = ImageIO.read(new File("src/main/java/com/bikeshare/gui/pic/logo.jpg"));
			JLabel logo = new JLabel(new ImageIcon(logoImage.getScaledInstance(200,200, Image.SCALE_SMOOTH)));
			add(logo);
			logo.setBounds(205,65,200,200);
		}catch(Exception e){
			e.printStackTrace();
		}




		emailLabel.setBounds(150, 280, 100, 33);
		email.setBounds(150, 315, 300, 33);
		passwordLabel.setBounds(150,350,400,33);
		password.setBounds(150,385,300,33);
		loginButton.setBounds(115, 460, 155, 33);
		registerButton.setBounds(330, 460, 155, 33);
		infoButton.setBounds(400, 20, 155, 33);

		add(emailLabel);
		add(email);
		add(passwordLabel);
		add(password);
		add(registerButton);
		add(loginButton);
		add(infoButton);

		setLayout(null);
		setVisible(true);

		getContentPane().setBackground(Color.WHITE);
		setSize(600,600);
		setLocation(400,100);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);//close on exit

		/*
		 * Check if email/password are valid. Display a dialog box if not.
		 */
		loginButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					boolean is_valid = customer.getCustomerDB().check_login(email.getText(), String.valueOf(password.getPassword()));

					if(!is_valid) {
						JOptionPane.showMessageDialog(null,"Incorrect Email address or Password");
						email.setText("");
						password.setText("");
						return;
					}
					// Init Customer
					customer.init(email.getText());
					// Re-direct user based on their qualifications
					String qualification = customer.getQualification();
					if (qualification.equalsIgnoreCase("user")) {
						new CustomerMenu().setVisible(true);
					}
					else if (qualification.equalsIgnoreCase("manager"))
					{
						new ManagerMenu().setVisible(true);
					}else if(qualification.equalsIgnoreCase("operator")){
						new OperatorMenu().setVisible(true);
					}
					dispose();
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});

		registerButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dispose();
				new Register().setVisible(true);
			}
		});

		infoButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				JFrame infoFrame = new JFrame();
				infoFrame.getContentPane().setLayout(new FlowLayout());
				JTextPane infoTxt = new JTextPane ();
				infoTxt.setContentType("text/html");
				infoTxt.setText("<html>" +
						"<h2> General information </h2>" +
						"<p><h3>Price plan:<h3>" +
						"1-30 days : 6$ per day.<br>" +
						"1-6 months : first month 140$ and 4$ per day.<br>" +
						">6 months : first 6 months 600$ and 3 $ per day.</p>" +
						"<p> <h3>Video Manual:</h3>" +
						"Visit <a href=\"\">https://youtu.be/ugX3jj1fR6g</a> for a video instruction.</p> " +
						"<h3>Contact Us:</h3>" +
						"<a href=\"\">customerservice@team1abike.com</a>" +
						"</html>");
				infoFrame.add(infoTxt);
				infoFrame.pack();
				infoFrame.setVisible(true);


			}
		});

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {}


}

