package com.bikeshare.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

public class ReportBrokenBike extends JFrame implements ActionListener {

    // GUI components
    JLabel bikeIdLabel = new JLabel("Bike Number");
    JComboBox bikeCombo = new JComboBox();
    JLabel detailLabel = new JLabel("Detail: ");
    JTextField detailText =new JTextField(40);
    JButton reportButton =new JButton("Report!");

    JLabel menuLabel = new JLabel("Report Bike");
    JButton backButton = new JButton("Back");
    JPanel topPanel = new JPanel();
    // Customer instance
    com.bikeshare.customers.CustomerImplem customer = init_app.menu.customer;
    Map<Integer, ArrayList<Integer>> bikes;
    public ReportBrokenBike(){

        setLayout(null);
        bikeIdLabel.setBounds(200,200,200,30);
        bikeCombo.setBounds(200,230,400,30);
        detailLabel.setBounds(200,300,200,30);
        detailText.setBounds(200,330,400,100);
        reportButton.setBounds(300,450,200,30);
        backButton.setBounds(600,30,100,30);
        menuLabel.setForeground(Color.decode("#f1f0ee"));
        menuLabel.setFont(new Font("Sans Serif", Font.BOLD,28));
        menuLabel.setBounds(10,10,200,30);
        menuLabel.setHorizontalAlignment(JLabel.CENTER);
        topPanel.setBounds(0,0,800,50);
        topPanel.setBackground(Color.decode("#269ccc"));
        topPanel.setLayout(null);
        backButton.setBounds(700,10,80,30);
        topPanel.add(backButton);
        topPanel.add(menuLabel);

        add(topPanel);
        add(bikeIdLabel);
        add(bikeCombo);
        add(detailLabel);
        add(detailText);
        add(reportButton);


        getContentPane().setBackground(Color.WHITE);
        setLocationByPlatform(true);
        setVisible(true);
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        initBikeOption();

        // update bike status to broken
        reportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                int bike_id = (int)bikeCombo.getSelectedItem();
                String details_of_damage=detailText.getText();
                customer.getCustomerDB().report_damaged_bike(bike_id,details_of_damage);
                displayOperationMessage();
            }
        });

        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new CustomerMenu().setVisible(true);
                dispose();
            }
        });
    }
    private void initBikeOption(){
        bikes = customer.getCustomerDB().get_available_bikes();
        // Add bike options to the combo box
        for(int locationId : bikes.keySet()){
            System.out.println(locationId);
            for(int bikeId : bikes.get(locationId)){
                System.out.println(bikeId);
                bikeCombo.addItem(bikeId);
            }
        }

        return;
    }
    // Display a dialog upon submitting a report
    private void displayOperationMessage(){
        JOptionPane.showMessageDialog( null,"Thank you! Your report has been received");
        new CustomerMenu().setVisible(true);
        dispose();

    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }
}
