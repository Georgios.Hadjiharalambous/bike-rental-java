package com.bikeshare.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class MoveBike extends JFrame implements ActionListener  {
    // GUI components
    JLabel locationLabel = new JLabel("Location");
    JComboBox locationCombo = new JComboBox();
    JLabel bikeIdLabel = new JLabel("Bike Number");
    JComboBox bikeCombo = new JComboBox();
    JLabel newLocationLabel = new JLabel("New Location");
    JComboBox newLocationCombo = new JComboBox();
    JButton moveButton =new JButton("Move Bike");
    JButton backButton = new JButton("Back");
    //Operator instant
    com.bikeshare.database.OperatorDB operator = init_app.menu.operator;
    //Map of location_id and bike_ids
    HashMap<Integer, ArrayList<Integer>>bikes = new HashMap<Integer, ArrayList<Integer>>();
    // Map of location_id and location name
    HashMap<Integer,String> locations;


    public MoveBike(){

        String instructionMessage = " - Please select a location - ";
        locationCombo.addItem(instructionMessage);
        initLocationInfo();
        initBikeInfo();
        setLayout(null);

        locationLabel.setBounds(200,100,200,30);
        locationCombo.setBounds(200,130,400,30);
        bikeIdLabel.setBounds(200,200,200,30);
        bikeCombo.setBounds(200,230,400,30);
        newLocationLabel.setBounds(200,350,400,30);
        newLocationCombo.setBounds(200,380,400,30);
        moveButton.setBounds(300,450,200,30);
        backButton.setBounds(700,10,80,30);

        add(locationLabel);
        add(locationCombo);
        add(bikeIdLabel);
        add(bikeCombo);
        add(newLocationLabel);
        add(newLocationCombo);
        add(moveButton);
        add(backButton);

        moveButton.setEnabled(false);

        setVisible(true);
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);//close on exit

        //
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new OperatorMenu().setVisible(true);
                dispose();
            }
        });
        // Display available bikes in the selected location
        locationCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                // If customer reselect the instruction message, clear bike combo-box
                bikeCombo.removeAllItems();
                moveButton.setEnabled(false);
                if(locationCombo.getSelectedIndex() == 0)return;

                String location_name = (String)locationCombo.getSelectedItem();
                int selected_location =  Integer.parseInt(location_name.substring(0,location_name.indexOf(":")));
                if(!bikes.containsKey(selected_location) || bikes.get(selected_location).size()==0) {
                    bikeCombo.addItem("-- No bike in selected location --");
                    return;
                }
                bikeCombo.removeAllItems();
                for(int bike_id : bikes.get(selected_location)){
                    bikeCombo.addItem(bike_id);
                }
                moveButton.setEnabled(true);
            }
        });
        //move bike to another location and update combobox accordingly
        moveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                int bike_id = (int) bikeCombo.getSelectedItem();
                String location_name = (String)newLocationCombo.getSelectedItem();
                int location_id = Integer.parseInt(location_name.substring(0,location_name.indexOf(":")));
                operator.change_bike_location(bike_id,location_id);
                JOptionPane.showMessageDialog(null, "The bike has been moved!");
                String lastLocationName = (String)locationCombo.getSelectedItem();
                int lastLocationID = Integer.parseInt(lastLocationName.substring(0,lastLocationName.indexOf(":")));
                bikes.get(lastLocationID).removeAll(Arrays.asList(bike_id));
                if(!bikes.containsKey(location_id)){
                    bikes.put(location_id, new ArrayList<Integer>());
                }
                bikes.get(location_id).add(bike_id);
                bikeCombo.removeAllItems();
                locationCombo.setSelectedIndex(0);

            }
        });

    }
    //get locations info from DB
    private void initLocationInfo(){
        locations = new HashMap<Integer, String>();
        HashMap<String, Object> locData = operator.get_locations_info();
        String[] columns = (String[])locData.get("columns");
        int locIdIdx = Arrays.asList(columns).indexOf("location id");
        int nameIdIdx = Arrays.asList(columns).indexOf("station_name");
        String[][] locationInfo = (String[][])locData.get("data");

        for(int i=0;i<locationInfo.length;i++){
            locationCombo.addItem(locationInfo[i][locIdIdx]+": "+locationInfo[i][nameIdIdx]);
            newLocationCombo.addItem(locationInfo[i][locIdIdx]+": "+locationInfo[i][nameIdIdx]);
            locations.put(Integer.parseInt(locationInfo[i][locIdIdx]),locationInfo[i][nameIdIdx]);

        }
    }
    // get bike info from DB
    private void initBikeInfo(){

        String[] columns = (String[])operator.get_bike_info().get("columns");
        int bikeIdIdx = Arrays.asList(columns).indexOf("bike id");
        int locationIdIdx = Arrays.asList(columns).indexOf("location id");

        String[][] bike_data = (String[][])operator.get_bike_info().get("data");
        for(int i=0;i<bike_data.length;i++){
            int currLocationId = Integer.parseInt(bike_data[i][locationIdIdx]);
            if(!bikes.containsKey(currLocationId)){
                bikes.put(currLocationId,new ArrayList<Integer>());
            }
            bikes.get(currLocationId).add(Integer.parseInt(bike_data[i][bikeIdIdx]));
        }

    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }
    // get locations and available bikes from the database then add to combo box
}
