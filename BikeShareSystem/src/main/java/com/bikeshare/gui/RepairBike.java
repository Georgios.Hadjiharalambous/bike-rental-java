package com.bikeshare.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class RepairBike extends JFrame implements ActionListener  {
    //GUI components
    JLabel damageIdLabel = new JLabel("Report Number");
    JLabel detailLabel = new JLabel("Report Detail:");
    JComboBox damageCombo = new JComboBox();
    JButton backButton = new JButton("Back");
    JButton repairButton =new JButton("Repair Bike");
    JTextArea damageDetail = new JTextArea("");
    //operator instance
    com.bikeshare.database.OperatorDB operator = init_app.menu.operator;
    // Map of damageID and a row of damage
    HashMap<Integer, String[]> damageMap = new HashMap<Integer, String[]>();
    public RepairBike(){

        String[][] damage_data = (String[][])operator.get_damage_info().get("data");
        String[] columns = (String[])operator.get_damage_info().get("columns");
        int damageIdx = Arrays.asList(columns).indexOf("damage_id");
        int detailIdx = Arrays.asList(columns).indexOf("damage_details");
        int bikeIdx = Arrays.asList(columns).indexOf("bike_id");
        // Add location options to the combo box
        String instructionMessage = " - Please select a report ID - ";
        damageCombo.addItem(instructionMessage);
        for(int i=0;i<damage_data.length;i++){
            damageMap.put(Integer.parseInt(damage_data[i][damageIdx]),damage_data[i]);
            damageCombo.addItem(Integer.parseInt(damage_data[i][damageIdx]));
        }

        setLayout(null);
        backButton.setBounds(700,10,80,30);
        repairButton.setBounds(300,350,200,30);
        damageCombo.setBounds(400,50,200,30);
        damageIdLabel.setBounds(200,50,200,30);
        detailLabel.setBounds(200,100, 200,30);
        damageDetail.setBounds(200,130,400,200);
        damageDetail.setBackground(Color.WHITE);
        add(detailLabel);
        add(damageIdLabel);
        add(damageCombo);
        add(repairButton);
        add(damageDetail);
        add(backButton);

        setLayout(new BorderLayout());
        setVisible(true);
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);//close on exit

        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new OperatorMenu().setVisible(true);
                dispose();
            }
        });
        // Display damage detail when damage_id is selected
        damageCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                damageDetail.setText("");
                if(damageCombo.getSelectedItem()== instructionMessage) return;
                int selectedId = (int)damageCombo.getSelectedItem();
                if(damageMap.containsKey(selectedId)){
                    damageDetail.setText(damageMap.get(selectedId)[detailIdx]);
                }
            }
        });
        // repair bike and change damage.fixed from 0 to 1
        repairButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                int damageId = (int)damageCombo.getSelectedItem();
                int bikeId = Integer.parseInt(damageMap.get(damageId)[bikeIdx]);
                operator.fix_damaged_bike(bikeId);
                operator.solve_report(damageId);
                damageCombo.removeItemAt(damageCombo.getSelectedIndex());
                JOptionPane.showMessageDialog(null, "The bike has been repaired!");
            }
        });

    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }
    // get locations and available bikes from the database then add to combo box
}
