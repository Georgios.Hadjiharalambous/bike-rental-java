package com.bikeshare.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

public class RentBike extends JFrame implements ActionListener {

    // GUI Components
    JLabel locationLabel = new JLabel("Renting from");
    JComboBox locationCombo = new JComboBox();
    JLabel bikeIdLabel = new JLabel("Bike Number");
    JComboBox bikeCombo = new JComboBox();
    JButton rentButton =new JButton("Rent Bike");

    JLabel menuLabel = new JLabel("Rent Bike");
    JButton backButton = new JButton("Back");
    JPanel topPanel = new JPanel();
    // customer instance
    com.bikeshare.customers.CustomerImplem customer = init_app.menu.customer;
    Map<Integer, ArrayList<Integer>>bikes;
    Map<Integer,String> locations;


    public RentBike(){

        setLayout(null);


        locationLabel.setBounds(200,200,200,30);
        locationCombo.setBounds(200,230,400,30);
        bikeIdLabel.setBounds(200,300,200,30);
        bikeCombo.setBounds(200,330,400,30);
        rentButton.setBounds(300,400,200,30);

        menuLabel.setForeground(Color.decode("#f1f0ee"));
        menuLabel.setFont(new Font("Sans Serif", Font.BOLD,28));
        menuLabel.setBounds(10,10,200,30);
        menuLabel.setHorizontalAlignment(JLabel.CENTER);
        topPanel.setBounds(0,0,800,50);
        topPanel.setBackground(Color.decode("#269ccc"));
        topPanel.setLayout(null);
        backButton.setBounds(700,10,80,30);
        topPanel.add(backButton);
        topPanel.add(menuLabel);

        add(topPanel);
        add(locationLabel);
        add(locationCombo);
        add(bikeIdLabel);
        add(bikeCombo);
        add(rentButton);
        //add(backButton);

        rentButton.setEnabled(false);

        getContentPane().setBackground(Color.WHITE);
        setLocationByPlatform(true);
        setVisible(true);
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        initBikeOption();

        // When user selects a location, display available bikes in that location
        locationCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                // If customer reselect the instruction message, clear bike combo-box
                if(locationCombo.getSelectedIndex() == 0){
                    bikeCombo.removeAllItems();
                    return;
                };
                String location_name = (String)locationCombo.getSelectedItem();
                int location_id = Integer.parseInt(location_name.substring(0,location_name.indexOf(":")));
                bikeCombo.removeAllItems();
                for(int bike_id : bikes.get(location_id)){
                    bikeCombo.addItem(bike_id);
                }
            }
        });
        // enable rent button when a bike is selected
        bikeCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if(bikeCombo.getSelectedIndex() != -1){
                    rentButton.setEnabled(true);
                }
            }
        });

        // Add a new rental and change bike status to 'rented'
        rentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

                int user_id = customer.getUser_id();
                int bike_id = (int) bikeCombo.getSelectedItem();
                String location_name = (String)locationCombo.getSelectedItem();
                int location_id = Integer.parseInt(location_name.substring(0,location_name.indexOf(":")));
                customer.getCustomerDB().add_rental(user_id, bike_id, location_id);
                customer.getCustomerDB().rent_a_bike(bike_id);
                customer.setIs_renting(true);
                customer.setRented_bike_id(bike_id);

                JOptionPane.showMessageDialog(null,"Your bike will be unlocked shortly. Enjoy the ride!");
                new CustomerMenu().setVisible(true);
                dispose();
            }
        });
        // back to the menu
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new CustomerMenu().setVisible(true);
                dispose();
            }
        });


    }
    // Get bikes and locations info from DB and insert locations where there is an available bike into the combobox
    private void initBikeOption(){

        locations = customer.getCustomerDB().get_location_ids_name();
        bikes = customer.getCustomerDB().get_available_bikes();
        // Add location options to the combo box
        String instructionMessage = " - Please select a location - ";
        locationCombo.addItem(instructionMessage);
        for(int location_id : bikes.keySet()){
            locationCombo.addItem(""+location_id+": "+locations.get(location_id));
        }

        return;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        return;
    }

}
