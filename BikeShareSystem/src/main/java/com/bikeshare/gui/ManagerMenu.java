package com.bikeshare.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.*;


import com.bikeshare.charts.BikesRentedVsLocations;
import com.bikeshare.charts.BikesReport;
import com.bikeshare.charts.RevenueVsMonth;

@SuppressWarnings("serial")
public class ManagerMenu extends JFrame implements ActionListener {

	private static final String BIKERENTEDVSLOCATIONS = "Bike Rented per Location";
	private static final String BIKESTAT = "Overall Bike Statistics";
	private static final String REVENUEVSMONTH = "Revenue per Month";
	
	private String selectedReport;

	JLabel reportLabel = new JLabel("Choose Report");
    JComboBox<String> reportComboBox = new JComboBox<>();

    JButton generateReportButton =new JButton("Generate Report");
    
    JButton logoutButton =new JButton("Logout");


    com.bikeshare.customers.CustomerImplem customer = init_app.menu.customer;
    Map<Integer, ArrayList<Integer>>bikes;
    public ManagerMenu(){
    	super("Manager");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setLayout(null);
    	setSize(600, 600);
        setLocationByPlatform(true);



        reportLabel.setBounds(100, 200, 100, 30);
        reportComboBox.setBounds(100, 230, 400, 30);
        generateReportButton.setBounds(200, 270, 175, 30);
        logoutButton.setBounds(220, 350, 130, 30);

        getContentPane().add(reportLabel);
        getContentPane().add(reportComboBox);
        getContentPane().add(generateReportButton);
        getContentPane().add(logoutButton);

        initialiseReports();
        
        
        reportComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
  
                selectedReport = (String)reportComboBox.getSelectedItem();
                
            }
        });

        generateReportButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {

                System.out.println(selectedReport);
                
                if(selectedReport.equalsIgnoreCase(BIKERENTEDVSLOCATIONS))
                {
                	new BikesRentedVsLocations();
                }
                else if(selectedReport.equalsIgnoreCase(BIKESTAT))
                {
                	new BikesReport();
                }
                else if(selectedReport.equalsIgnoreCase(REVENUEVSMONTH))
                {
                	new RevenueVsMonth();
                }
            }
        });

        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
                new Menu().setVisible(true);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }
    
    public void initialiseReports(){
        reportComboBox.removeAllItems();
        String instructionMessage = " - Please select a report - ";
        reportComboBox.addItem(instructionMessage);
        reportComboBox.addItem(BIKERENTEDVSLOCATIONS);
        reportComboBox.addItem(BIKESTAT);
        reportComboBox.addItem(REVENUEVSMONTH);
    }
}
