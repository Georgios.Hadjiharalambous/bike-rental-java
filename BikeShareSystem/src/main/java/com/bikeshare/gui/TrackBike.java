package com.bikeshare.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class TrackBike extends JFrame implements ActionListener  {
    // GUI components
    JButton backButton = new JButton("Back");
    JLabel trackLabel = new JLabel("Bike Location and Status");
    JTable bikeTable;
    JScrollPane scrollPane;
    // Operator Instance
    com.bikeshare.database.OperatorDB operator = init_app.menu.operator;
    // Map of location_id and location name
    HashMap<Integer,String> locations;

    public TrackBike(){

        trackLabel.setFont(new Font(Font.SANS_SERIF, Font.ITALIC,18));
        setBackground(Color.WHITE);
        setLayout(null);
        //get locations from DB and populate the combobox
        initLocationInfo();
        //Create a JTable to show bike locations
        initBikeTable();
        // JTable to scrollable pane
        scrollPane = new JScrollPane(bikeTable);

        backButton.setBounds(700,10,80,30);
        trackLabel.setBounds(10,10,500,30);
        bikeTable.setBounds(30, 40, 200, 300);
        scrollPane.setBounds(100,100,600,400);

        add(scrollPane);
        add(backButton);
        add(trackLabel);

        setLayout(new BorderLayout());
        setVisible(true);
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);//close on exit

        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new OperatorMenu().setVisible(true);
                dispose();
            }
        });

    }
    // Get locations from DB and put them in map
    private void initLocationInfo(){
        locations = new HashMap<Integer, String>();
        HashMap<String, Object> locData = operator.get_locations_info();
        String[] columns = (String[])locData.get("columns");
        int locIdIdx = Arrays.asList(columns).indexOf("location id");
        int nameIdIdx = Arrays.asList(columns).indexOf("station_name");
        String[][] locationInfo = (String[][])locData.get("data");
        for(int i=0;i<locationInfo.length;i++){
            locations.put(Integer.parseInt(locationInfo[i][locIdIdx]),locationInfo[i][nameIdIdx]);
        }
    }
    // Create Table odf bike info , replace location_id with station name
    private void initBikeTable(){
        HashMap<String, Object> data = operator.get_bike_info();
        String[][] bikeData = (String[][]) data.get("data");
        String[] columns = (String[]) data.get("columns");
        int damageIdx = Arrays.asList(columns).indexOf("damaged");
        int rentedIdx = Arrays.asList(columns).indexOf("rented");
        int locationIdIdx = Arrays.asList(columns).indexOf("location id");
        for(int i=0;i<bikeData.length;i++){
            if(bikeData[i][damageIdx] == "0") bikeData[i][damageIdx] ="No";
            if(bikeData[i][damageIdx] == "1") bikeData[i][damageIdx] ="Yes";
            if(bikeData[i][rentedIdx] == "0") bikeData[i][damageIdx] ="No";
            if(bikeData[i][rentedIdx] == "1") bikeData[i][damageIdx] ="Yes";
            bikeData[i][locationIdIdx] = locations.get(Integer.parseInt(bikeData[i][locationIdIdx]));
        }
        columns[locationIdIdx] = "Location(Station Name)";
        bikeTable = new JTable(bikeData,columns);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }
    // get locations and available bikes from the database then add to combo box
}
