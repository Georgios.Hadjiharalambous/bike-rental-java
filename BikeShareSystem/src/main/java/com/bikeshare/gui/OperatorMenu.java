package com.bikeshare.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import com.bikeshare.operators.Operator;

public class OperatorMenu extends JFrame implements ActionListener {

    JButton trackButton =new JButton("Track Bike");
    JButton repairButton =new JButton("Repair");
    JButton moveButton =new JButton("Move");
    JButton logoutButton =new JButton("Logout");

    public OperatorMenu() {

        setLayout(null);

        logoutButton.setBounds(650,30,80,30);
        trackButton.setBounds(250,100,300,80);
        repairButton.setBounds(250,250,300,80);
        moveButton.setBounds(250,400,300,80);

        add(logoutButton);
        add(trackButton);
        add(repairButton);
        add(moveButton);

        setLocationByPlatform(true);
        setVisible(true);
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);


        trackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
                new TrackBike().setVisible(true);
            }
        });
        repairButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
                new RepairBike().setVisible(true);
            }
        });
        moveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
                new MoveBike().setVisible(true);
            }
        });
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
                new Menu().setVisible(true);
            }
        });


    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Register().setVisible(true);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("");
    }
}
