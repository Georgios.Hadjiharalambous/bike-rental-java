package com.bikeshare.managers;

public interface Manager {

	/**
	 * Generate reports showing all bike activities over a defined time period,
	 * using appropriate data visualization techniques.
	 */
	public void generateReports();

}
