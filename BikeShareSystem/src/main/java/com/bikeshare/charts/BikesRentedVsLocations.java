package com.bikeshare.charts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.RefineryUtilities;

import com.bikeshare.database.DBConnection;

public class BikesRentedVsLocations extends JFrame {

	Map<Integer, String> locationDetails = new HashMap<>();
	Map<Integer, Integer> rentedLocationCount = new HashMap<>();
	public final static String TITLE = "Bike Rental System";

	public BikesRentedVsLocations() {
		super(TITLE);
		setSize(600, 400);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		getDataFromDB();

		setContentPane(createDemoPanel());

		RefineryUtilities.centerFrameOnScreen(this);

		this.setVisible(true);
	}

	private PieDataset createDataset() {
		DefaultPieDataset dataset = new DefaultPieDataset();

		for (Integer locationId : locationDetails.keySet()) {
			String location = locationDetails.get(locationId);
			Integer rentalCount = rentedLocationCount.get(locationId);
			if (rentalCount == null) {
				rentalCount = 0;
			}

			dataset.setValue(location, rentalCount);
		}
		return dataset;
	}

	private static JFreeChart createChart(PieDataset dataset) {
		JFreeChart chart = ChartFactory.createPieChart("Bikes Rented Vs Locations", // chart title
				dataset, // data
				true, // include legend
				true, false);

		return chart;
	}

	public JPanel createDemoPanel() {
		JFreeChart chart = createChart(createDataset());
		return new ChartPanel(chart);
	}

	private void getDataFromDB() {
		Connection connection = DBConnection.create_connection_with_db();
		if (connection != null) {
			try {
				String locationQuery = " Select location_id,station_name from locations ";
				PreparedStatement locationStatement = connection.prepareStatement(locationQuery);
				ResultSet locationResult = locationStatement.executeQuery(locationQuery);
				System.out.println(locationResult);

				while (locationResult.next()) {
					Integer locationId = locationResult.getInt("location_id");
					String stationName = locationResult.getString("station_name");
					locationDetails.put(locationId, stationName);

				}
				System.out.println(locationDetails);

				String rentedLocationQuery = " Select location_id_start from rentals ";
				PreparedStatement rentedLocationStatement = connection.prepareStatement(rentedLocationQuery);
				ResultSet rentedLocationResult = rentedLocationStatement.executeQuery(rentedLocationQuery);

				while (rentedLocationResult.next()) {
					Integer locationStartId = rentedLocationResult.getInt("location_id_start");

					if (rentedLocationCount.containsKey(locationStartId)) {
						int count = rentedLocationCount.get(locationStartId);
						rentedLocationCount.put(locationStartId, ++count);
					} else {
						rentedLocationCount.put(locationStartId, 1);
					}
				}

				System.out.println(rentedLocationCount);

				connection.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
//   public static void main( String[ ] args ) {
//	   BikesRentedVsLocations demo = new BikesRentedVsLocations();
//   }
}