package com.bikeshare.charts;

import org.jfree.chart.ChartPanel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.RefineryUtilities;

import com.bikeshare.database.DBConnection;

import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class RevenueVsMonth extends JFrame {

	private Map<String, Float> revenueInMonths = new TreeMap<>();

	public final static String APP_TITLE = "Bike Rental System";
	public final static String CHART_TITLE = "Revenue Report";

	public RevenueVsMonth() {
		super(APP_TITLE);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		getDataFromDB();
		JFreeChart lineChart = ChartFactory.createLineChart(CHART_TITLE, "Month", "Revenue in Pounds", createDataset(),
				PlotOrientation.VERTICAL, true, true, false);

		ChartPanel chartPanel = new ChartPanel(lineChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(600, 400));
		setContentPane(chartPanel);

		this.pack();
		RefineryUtilities.centerFrameOnScreen(this);
		this.setVisible(true);
	}

	private void getDataFromDB() {
		Connection connection = DBConnection.create_connection_with_db();
		if (connection != null) {
			try {
				String revenueQuery = " select DATE_FORMAT(date_end,\"%Y-%m\"), money_cost from rentals ";
				PreparedStatement revenueStatement = connection.prepareStatement(revenueQuery);
				ResultSet revenueResult = revenueStatement.executeQuery(revenueQuery);
				System.out.println(revenueResult);

				while (revenueResult.next()) {
					String month = revenueResult.getString(1);
					Float money = revenueResult.getFloat(2);
					if (revenueInMonths.containsKey(month)) {
						Float cost = revenueInMonths.get(month);
						money += cost;
						revenueInMonths.put(month, money);

					} else {
						revenueInMonths.put(month, money);

					}
				}
				System.out.println(revenueInMonths);

				connection.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	private DefaultCategoryDataset createDataset() {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		final String REVENUE = "Revenue in Pounds";

		for (String month : revenueInMonths.keySet()) {
			dataset.addValue(revenueInMonths.get(month), REVENUE, month);
		}
		return dataset;
	}

//   public static void main( String[ ] args ) {
//	   RevenueVsMonth chart = new RevenueVsMonth();
//   }
}