package com.bikeshare.charts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

import com.bikeshare.database.DBConnection;

public class BikesReport extends JFrame {

	private Map<String, Integer> rentalMonthCount = new HashMap<>();
	private Map<String, Integer> repairMonthCount = new HashMap<>();
	private Map<String, Integer> damageMonthCount = new HashMap<>();

	public final static String APP_TITLE = "Bike Rental System";
	public final static String CHART_TITLE = "Bikes Report";

	public BikesReport() {

		super(APP_TITLE);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		getDataFromDB();

		JFreeChart barChart = ChartFactory.createBarChart(CHART_TITLE, "Month", "Bike Units", createDataset(),
				PlotOrientation.VERTICAL, true, true, false);

		ChartPanel chartPanel = new ChartPanel(barChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(600, 400));

		setContentPane(chartPanel);

		this.pack();

		RefineryUtilities.centerFrameOnScreen(this);

		this.setVisible(true);
	}

	private Set<String> getTotalMonths() {
		Set<String> monthsSet = new TreeSet<>();
		for (String month : rentalMonthCount.keySet()) {
			monthsSet.add(month);
		}
		for (String month : repairMonthCount.keySet()) {
			monthsSet.add(month);
		}
		for (String month : damageMonthCount.keySet()) {
			monthsSet.add(month);
		}

		return monthsSet;
	}

	private CategoryDataset createDataset() {
		System.out.println(rentalMonthCount);
		System.out.println(repairMonthCount);

		System.out.println(damageMonthCount);

		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		final String RENT = "RENT";
		final String REPAIR = "REPAIR";
		final String DAMAGE = "DAMAGE";

		for (String month : getTotalMonths()) {
			Integer rentCount = rentalMonthCount.get(month);
			if (rentCount == null) {
				rentCount = 0;
			}
			dataset.addValue(rentCount, RENT, month);

			Integer repairCount = repairMonthCount.get(month);
			if (repairCount == null) {
				repairCount = 0;
			}
			dataset.addValue(repairCount, REPAIR, month);

			Integer damageCount = damageMonthCount.get(month);
			if (damageCount == null) {
				damageCount = 0;
			}
			dataset.addValue(damageCount, DAMAGE, month);

		}

		return dataset;
	}

	private void getDataFromDB() {
		Connection connection = DBConnection.create_connection_with_db();
		if (connection != null) {
			try {
				String rentalsGroupedByMonthQuery = "select DATE_FORMAT(date_start,\"%Y-%m\"), count(*) from rentals group by DATE_FORMAT(date_start, \"%Y-%m\") ";
				PreparedStatement rentalsGroupedByMonthPS = connection.prepareStatement(rentalsGroupedByMonthQuery);
				ResultSet rentalsGroupedByMonthRS = rentalsGroupedByMonthPS.executeQuery(rentalsGroupedByMonthQuery);

				while (rentalsGroupedByMonthRS.next()) {
					String month = rentalsGroupedByMonthRS.getString(1);
					Integer rentCount = rentalsGroupedByMonthRS.getInt(2);
					rentalMonthCount.put(month, rentCount);
				}

				String repairsGroupedByMonthQuery = "select DATE_FORMAT(repair_date,\"%Y-%m\"), count(*) from repair group by DATE_FORMAT(repair_date, \"%Y-%m\") ";
				PreparedStatement repairsGroupedByMonthPS = connection.prepareStatement(repairsGroupedByMonthQuery);
				ResultSet repairsGroupedByMonthRS = repairsGroupedByMonthPS.executeQuery(repairsGroupedByMonthQuery);

				while (repairsGroupedByMonthRS.next()) {
					String month = repairsGroupedByMonthRS.getString(1);
					Integer repairCount = repairsGroupedByMonthRS.getInt(2);
					repairMonthCount.put(month, repairCount);
				}

				String damagesGroupedByMonthQuery = "select DATE_FORMAT(damage_date,\"%Y-%m\"), count(*) from damage group by DATE_FORMAT(damage_date, \"%Y-%m\");";
				PreparedStatement damagesGroupedByMonthPS = connection.prepareStatement(damagesGroupedByMonthQuery);
				ResultSet damagesGroupedByMonthRS = damagesGroupedByMonthPS.executeQuery(damagesGroupedByMonthQuery);

				while (damagesGroupedByMonthRS.next()) {
					String month = damagesGroupedByMonthRS.getString(1);
					Integer damageCount = damagesGroupedByMonthRS.getInt(2);
					damageMonthCount.put(month, damageCount);
				}

			} catch (SQLException exception) {
				exception.printStackTrace();
			}
		}
	}
//	public static void main(String[] args) {
//		BikesReport chart = new BikesReport();
//	}
}