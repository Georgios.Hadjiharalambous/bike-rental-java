-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: bikerental
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bikes`
--

DROP TABLE IF EXISTS `bikes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bikes` (
  `bike_id` int(20) NOT NULL AUTO_INCREMENT,
  `rented` tinyint(1) NOT NULL DEFAULT '0',
  `damaged` tinyint(1) NOT NULL DEFAULT '0',
  `current_location_id` int(20) NOT NULL,
  PRIMARY KEY (`bike_id`),
  KEY `current_location_id` (`current_location_id`),
  CONSTRAINT `bikes_ibfk_1` FOREIGN KEY (`current_location_id`) REFERENCES `locations` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bikes`
--

LOCK TABLES `bikes` WRITE;
/*!40000 ALTER TABLE `bikes` DISABLE KEYS */;
INSERT INTO `bikes` VALUES (1,0,0,1),(2,0,0,2),(3,0,1,1),(4,0,0,4),(5,0,0,5),(6,0,1,10),(7,0,0,7),(8,0,0,8),(9,0,0,9),(10,0,0,3),(11,0,0,10),(12,0,0,6),(13,0,0,1),(14,0,0,2),(15,0,0,3),(16,0,0,4),(17,0,0,5),(18,0,0,6),(19,0,0,7),(20,0,0,1),(21,0,0,9),(22,0,0,10),(23,0,0,4),(24,0,0,5),(25,0,0,6),(26,0,0,8);
/*!40000 ALTER TABLE `bikes` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `delete_damage` BEFORE DELETE ON `bikes` FOR EACH ROW DELETE FROM damage WHERE damage.bike_id=id */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `delete_rentals_bikes` BEFORE DELETE ON `bikes` FOR EACH ROW DELETE FROM rentals WHERE rentals.bike_id=id */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `delete_repairs` BEFORE DELETE ON `bikes` FOR EACH ROW DELETE FROM repair WHERE repair.bike_id=id */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `damage`
--

DROP TABLE IF EXISTS `damage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `damage` (
  `damage_id` int(11) NOT NULL AUTO_INCREMENT,
  `bike_id` int(11) NOT NULL,
  `damage_date` date NOT NULL,
  `damage_details` text NOT NULL,
  `fixed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`damage_id`),
  KEY `bike_id` (`bike_id`),
  CONSTRAINT `damage_ibfk_1` FOREIGN KEY (`bike_id`) REFERENCES `bikes` (`bike_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `damage`
--

LOCK TABLES `damage` WRITE;
/*!40000 ALTER TABLE `damage` DISABLE KEYS */;
INSERT INTO `damage` VALUES (1,2,'2019-10-17','broken tyre',1),(2,1,'2019-09-03','brake failure',1),(3,3,'2019-08-03','chain lubrication',1),(4,1,'2019-11-01','Chain problem',1),(5,6,'2019-11-01','',0),(6,3,'2019-11-04','The wheels fell off',0);
/*!40000 ALTER TABLE `damage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `locations` (
  `location_id` int(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(20) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `station_name` varchar(100) NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'glasgow','123','Patrick'),(2,'glasgow','456','University of Glasgow'),(3,'glasgow','789','Hillhead'),(4,'glasgow','1011','Glasgow Central'),(5,'glasgow','1','Buchanan Street'),(6,'glasgow','1','Govan'),(7,'glasgow','1','Cowcaddens'),(8,'glasgow','1','Queens Park'),(9,'glasgow','1','Merchant City'),(10,'glasgow','1','Maryhill');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `delete_bikes_for_location` BEFORE DELETE ON `locations` FOR EACH ROW DELETE FROM bikes WHERE bikes.current_location_id=id */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `delete_rentals` BEFORE DELETE ON `locations` FOR EACH ROW DELETE FROM rentals WHERE rentals.location_id_start=id or rentals.location_id_end=id */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `rentals`
--

DROP TABLE IF EXISTS `rentals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rentals` (
  `rental_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bike_id` int(11) NOT NULL,
  `location_id_start` int(11) NOT NULL,
  `location_id_end` int(11) DEFAULT NULL,
  `date_start` date NOT NULL,
  `date_end` date DEFAULT '0000-01-01',
  `money_cost` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`rental_id`),
  KEY `user_id` (`user_id`),
  KEY `location_id_start` (`location_id_start`),
  KEY `location_id_end` (`location_id_end`),
  KEY `bike_id` (`bike_id`),
  CONSTRAINT `rentals_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `rentals_ibfk_3` FOREIGN KEY (`location_id_start`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `rentals_ibfk_4` FOREIGN KEY (`location_id_end`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `rentals_ibfk_5` FOREIGN KEY (`bike_id`) REFERENCES `bikes` (`bike_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rentals`
--

LOCK TABLES `rentals` WRITE;
/*!40000 ALTER TABLE `rentals` DISABLE KEYS */;
INSERT INTO `rentals` VALUES (1,1,1,1,1,'2019-10-03','2019-10-03',5),(2,2,1,2,1,'2019-10-02','2019-11-20',50),(3,3,2,1,2,'2019-10-26','2019-11-30',70),(4,4,2,1,3,'2019-09-14','2019-12-23',40),(5,9,1,2,1,'2019-10-17','2019-10-17',10),(6,9,2,2,1,'2019-10-17','2019-10-17',5),(7,12,1,1,3,'2019-08-01','2019-08-11',30),(8,12,6,3,3,'2019-08-12','2019-08-24',50),(9,12,2,4,4,'2019-09-01','2019-09-11',40),(10,12,2,4,3,'2019-09-12','2019-09-22',35),(11,12,1,3,1,'2019-11-01','2019-11-01',6),(12,12,3,2,1,'2019-11-01','2019-11-01',6),(13,15,2,3,2,'2019-11-04','2019-11-04',6),(14,15,1,1,4,'2019-11-04','2019-11-04',6),(15,19,17,5,5,'2019-11-05','2019-11-05',6),(16,19,17,5,5,'2019-11-05','2019-11-05',6),(17,19,11,6,6,'2019-11-05','2019-11-05',6),(18,19,26,7,8,'2019-11-05','2019-11-05',6),(19,19,20,8,1,'2019-11-05','2019-11-05',6),(20,19,21,9,9,'2019-11-05','2019-11-05',6),(21,19,22,10,10,'2019-11-05','2019-11-05',6),(22,19,11,6,10,'2019-11-05','2019-11-05',6),(23,19,2,2,2,'2019-11-05','2019-11-05',6),(24,19,2,2,2,'2019-11-05','2019-11-05',6);
/*!40000 ALTER TABLE `rentals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repair`
--

DROP TABLE IF EXISTS `repair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `repair` (
  `repair_id` int(11) NOT NULL AUTO_INCREMENT,
  `bike_id` int(11) NOT NULL,
  `repair_date` date NOT NULL,
  PRIMARY KEY (`repair_id`),
  KEY `bike_id` (`bike_id`),
  CONSTRAINT `repair_ibfk_1` FOREIGN KEY (`bike_id`) REFERENCES `bikes` (`bike_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repair`
--

LOCK TABLES `repair` WRITE;
/*!40000 ALTER TABLE `repair` DISABLE KEYS */;
INSERT INTO `repair` VALUES (1,1,'2019-10-03'),(2,2,'2019-09-03'),(3,2,'2019-11-01'),(4,3,'2019-11-01'),(5,1,'2019-11-01'),(6,1,'2019-11-05');
/*!40000 ALTER TABLE `repair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int(40) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) NOT NULL,
  `telephone_number` bigint(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `qualification` varchar(20) NOT NULL DEFAULT 'user',
  `money_credits` int(20) NOT NULL DEFAULT '0',
  `money_due` int(20) NOT NULL DEFAULT '0',
  `age` int(5) NOT NULL DEFAULT '0',
  `password` varchar(40) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Georgios Hadjiharalambous',99946329,'2486083h@student.gla.ac.uk','manager',0,0,0,'food'),(2,'Edo kai ekei',29,'gxxshumi','user',0,162,25,'edo'),(3,'Sou kiiiii',121189,'gxxshumi@nfksgdfgfd','user',0,0,25,'44d6feb3466b23a14b1e4f10719365d0e99c'),(4,'Sou kiiiii',354545,'gxxshumi@nfksgdfgfd','user',0,0,25,'44d6feb3466b23a14b1e4f10719365d0e99c'),(5,'Sou kiiiii',354545999,'gxxshumi@nfksgdfgfd','user',0,0,25,'44d6feb3466b23a14b1e4f10719365d0e99c'),(6,'Sou kiiiii',354549,'gxxshumi@nfksgdfgfd','user',0,0,25,'efb2501bff8f724d3ad9a38de5a7625aea66f48a'),(7,'kkkk',1,'esi@gg.c','user',0,0,22,'86e95a2849e3e6a5b5e657b8b836b4cf2f1884'),(8,'kokosxa',9,'g','manager',35,0,9,'ade7c2cf97f75d09975f4d72d1fa6c19f4897'),(9,'99',9,'9','user',28,0,9,'ade7c2cf97f75d09975f4d72d1fa6c19f4897'),(10,'44',46544654567878,'4','user',0,0,4,'1b6453892473a467d07372d45eb05abc2031647a'),(11,'44',44444444444444,'4','user',0,0,4,'1b6453892473a467d07372d45eb05abc2031647a'),(12,'YokeshJayasekar',12345,'yokeshm@gmail.com','manager',0,20,6021994,'8cb2237d679ca88db6464eac6da96345513964'),(13,'YokeshJ',12345,'yokeshc@gmail.com','user',0,0,25,'8cb2237d679ca88db6464eac6da96345513964'),(14,'YokeshJ',12345,'yokesho@gmail.com','operator',0,0,25,'8cb2237d679ca88db6464eac6da96345513964'),(15,'aa',1,'a','manager',4,0,1,'86f7e437faa5a7fce15d1ddcb9eaeaea377667b8'),(16,'customer',7111111111,'customer@customer.com','user',0,0,18,'5baa61e4c9b93f3f68225b6cf8331b7ee68fd8'),(17,'operator',711111111,'operator@operator.com','operator',0,0,22,'5baa61e4c9b93f3f68225b6cf8331b7ee68fd8'),(18,'manager',711111111,'manager@manager.com','manager',0,0,35,'5baa61e4c9b93f3f68225b6cf8331b7ee68fd8'),(19,'krittanaik',123,'krittanai@krittanai','user',9940,0,99,'8cb2237d679ca88db6464eac6da96345513964');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `delete_rentals_user` BEFORE DELETE ON `users` FOR EACH ROW DELETE from rentals where rentals.user_id=id */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'bikerental'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-05 23:51:56
