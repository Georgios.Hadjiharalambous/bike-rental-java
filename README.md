README

You can see the results of this project in the following videos :

1. [user journey](https://www.youtube.com/watch?v=ugX3jj1fR6g)
2. [operator journey](https://www.youtube.com/watch?v=G-RAFdlJE0k)
3. [manager journey](https://www.youtube.com/watch?v=xobKC4Cak1s)

===========================================================================
The Project source folder consists three sub folders:
1.	BikeShareSystem
2.	database_files
3.	external jars

===========================================================================


To run this project,

1.	Create a database called BikeRental in mysql8. Import the BikeRental.sql file in 
	database_files folder into BikeRental database. The username and password should 
	be root.
		<connect to mysql cli>
		create database BikeRental;
		<exit cli>
		mysql -u [root] -p -h localhost BikeRental < BikeRental.sql
		(At password prompt, enter root.)

2.	(Only if needed)If mysql is setup with different username and password, the same 
	should be updated in DBConnection.java for the project to connect to mysql.

		File:BikeShareSystem/src/main/java/com/bikeshare/database/DBConnection.java
		Modify this line with mysql credentials - <user> and <password>
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/BikeRental","<user>", "<password>");

3.	Import the java project BikeShareSystem into eclipse. The classpath to external libs 
	folder is relative and there should not be any compilation error.

4.	Run init_app.java in eclipse.

	File: BikeShareSystem/src/main/java/com/bikeshare/gui/init_app.java

5.	You can login as any of three users below or register a new customer.

		Customer: 
			Email: customer@customer.com
			Password: password
		Operator: 
			Email: operator@operator.com
			Password: password
		Manager: 
			Email: manager@manager.com
			Password: password
===========================================================================

